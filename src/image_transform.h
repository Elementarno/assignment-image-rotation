#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_TRANSFORM_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_TRANSFORM_H

#include "image.h"

struct image rotate90( struct image const* source ); // Clockwise rotation

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_TRANSFORM_H
