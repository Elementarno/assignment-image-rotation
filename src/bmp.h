#ifndef _BMP_H_
#define _BMP_H_

#include <stdint.h>
#include <stdbool.h>

#include "image.h"

enum read_status  {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_NOT_FOUND,
    READ_INVALID_PATH
};

enum read_status from_bmp( FILE* in, struct image* img );

enum read_status open_bmp(const char* path, struct image* img);

enum write_status  {
    WRITE_OK = 0,
    WRITE_STREAM_ERROR,
    WRITE_ERROR,
    WRITE_INVALID_PATH
};

enum write_status to_bmp( FILE* out, struct image const* img );

enum write_status save_bmp(const char* path, struct image* img);

#endif
