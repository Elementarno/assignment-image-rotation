#include <malloc.h>

#include "image.h"

struct pixel new_pixel(uint8_t b, uint8_t g, uint8_t r) {
    return (struct pixel) {.b = b, .g = g, .r = r};
}

struct image new_image(uint64_t width, uint64_t height, struct pixel* data) {
    return (struct image) {.width = width, .height = height, .data = data};
}

struct image new_image_empty(uint64_t width, uint64_t height) {
    return new_image(width, height, calloc(width*height, sizeof(struct pixel)));
}

void free_image(struct image* img) {
    free(img->data);
}

void set_pixel(struct image* img, uint64_t x, uint64_t y, struct pixel const p) {
    img->data[y*img->width + x] = p;
}

struct pixel* get_pixel(struct image const* img, uint64_t x, uint64_t y) {
    return &img->data[y*img->width + x];
}

void print_image_data(struct image const* img) {
    for (size_t y = 0; y < img->height; y++) {
        for (size_t x = 0; x < img->width; x++) {
            struct pixel* p = get_pixel(img, x, y);
            printf("(%3d,%3d,%3d) ", p->b, p->g, p->r);
        }
        printf("\n");
    }
}