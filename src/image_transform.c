#include "image.h"

struct image rotate90( struct image const* source ) {
    struct image result = new_image_empty(source->height, source->width);

    for (uint64_t x = 0; x < source->width; x++) {
        for (uint64_t y = 0; y < source->height; y++) {
            set_pixel(&result, y, (result.height - 1) - x, *get_pixel(source, x, y));
        }
    }

    return result;
}