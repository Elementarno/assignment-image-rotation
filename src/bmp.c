#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "bmp.h"

#define FOR_BMP_HEADER( FOR_FIELD ) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD( t, n ) t n ;

struct __attribute__((packed)) bmp_header
{
    FOR_BMP_HEADER( DECLARE_FIELD )
};

#define BF_TYPE 19778
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24

#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define PRINT_FIELD( t, name ) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");

void bmp_header_print( struct bmp_header const* header, FILE* f ) {
    FOR_BMP_HEADER( PRINT_FIELD )
}

static bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

static inline size_t padding(uint64_t width) {
    return width % 4 == 0 ? 0 : 4 - (width * sizeof(struct pixel)) % 4;
}

static struct bmp_header create_bmp_header(struct image const* img) {
    struct bmp_header header = {0};

    header.bfType = BF_TYPE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biSize = BI_SIZE;
    header.biBitCount = BI_BIT_COUNT;
    header.biPlanes = BI_PLANES;
    header.bOffBits = sizeof(struct bmp_header);

    return header;
}

static bool check_bmp_header(struct bmp_header const* header) {
    if (header->bfType != BF_TYPE) return false;
    if (header->bOffBits != sizeof(struct bmp_header)) return false;
    if (header->biWidth <= 0 || header->biHeight <= 0) return false;
    if (header->biSize != BI_SIZE) return false;
    if (header->biBitCount != BI_BIT_COUNT) return false;
    if (header->biPlanes != BI_PLANES) return false;

    return true;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};
    if (!read_header(in, &header) || !check_bmp_header(&header)) return READ_INVALID_HEADER;

    *img = new_image_empty(header.biWidth, header.biHeight);
    size_t extra_bytes = padding(header.biWidth);
    for (uint64_t y = 0; y < img->height; y++) {
        if (!fread(img->data + y * img->width, sizeof(struct pixel), img->width, in)) {
            free_image(img);
            return READ_INVALID_BITS;
        }
        fseek(in, extra_bytes, SEEK_CUR);
    }

    return READ_OK;
}

enum read_status open_bmp(const char* path, struct image* img) {
    if (!path || !(*path)) return READ_INVALID_PATH;
    FILE* f = fopen(path, "rb");
    if (!f) return READ_NOT_FOUND;

    enum read_status result = from_bmp(f, img);
    fclose(f);
    return result;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    if (!out) return WRITE_ERROR;

    struct bmp_header const header = create_bmp_header(img);
    if(!fwrite(&header, sizeof(header), 1, out)) {
        return WRITE_ERROR;
    }
    struct pixel p_empty = {0};

    size_t extra_bytes = padding(img->width);
    for (uint64_t y = 0; y < img->height; y++) {
        if (!fwrite(img->data + y * img->width, sizeof(struct pixel), img->width, out)) {
            return WRITE_ERROR;
        }
        if (extra_bytes && !fwrite(&p_empty, sizeof(uint8_t), extra_bytes, out)) {
            return WRITE_ERROR;
        };
    }

    return WRITE_OK;
}

enum write_status save_bmp(char const* path, struct image* img) {
    if (!path || !(*path)) return WRITE_INVALID_PATH;
    FILE* f = fopen(path, "wb");
    if (!f) return WRITE_STREAM_ERROR;

    enum write_status result = to_bmp(f, img);
    fclose(f);
    return result;
}