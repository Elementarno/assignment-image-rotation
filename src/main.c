#include "bmp.h"
#include "util.h"
#include "image_transform.h"

void usage() {
    fprintf(stderr, "Usage: ./print_header BMP_FILE_NAME\n"); 
}

char* const read_messages[] = {
        [READ_INVALID_BITS] = "Failed to read all bites.\n",
        [READ_NOT_FOUND] = "File not found.\n",
        [READ_INVALID_HEADER] = "Header is invalid.\n",
        [READ_INVALID_PATH] = "Path is invalid.\n",
};

char* const write_messages[] = {
        [WRITE_STREAM_ERROR] = "Some error occurred while file stream was creating.\n",
        [WRITE_ERROR] = "Some error occured trying writing data in file.\n",
        [WRITE_INVALID_PATH] = "Path is invalid.\n",
};

int main( int argc, char** argv ) {
    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments.\n" );
    if (argc > 2) err("Too many arguments.\n" );

    struct image img;
    char* path = argv[1];

    enum read_status rs = open_bmp(path, &img);
    if (rs != READ_OK) err(read_messages[rs]);

    /*
    switch (open_bmp(path, &img)) {
        case READ_INVALID_PATH: err("Path is invalid.\n");
        case READ_NOT_FOUND: err("File not found.\n");
        case READ_INVALID_HEADER: err("Header is invalid.\n");
        case READ_INVALID_BITS: err("Failed to read all bites.\n");
        case READ_OK: break;
    }*/

    struct image i_rot = rotate90(&img);
    enum write_status ws = save_bmp(path, &i_rot);
    if (ws != WRITE_OK) err(write_messages[ws]);
    /*switch (save_bmp(path, &i_rot)) {
        case WRITE_INVALID_PATH: err("Path is invalid.\n");
        case WRITE_ERROR: err("Some error occurred while file stream was creating.\n");
        case READ_OK: break;
    }*/

    return 0;
}
