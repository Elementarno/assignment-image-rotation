#ifndef _UTIL_H_
#define _UTIL_H_

#include "image.h"

_Noreturn void err( const char* msg, ... );

#endif
