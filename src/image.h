#ifndef VIEW_HEADER_IMAGE_H
#define VIEW_HEADER_IMAGE_H

#include <stdio.h>
#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel new_pixel(uint8_t b, uint8_t g, uint8_t r);
struct image new_image(uint64_t width, uint64_t height, struct pixel* data);
struct image new_image_empty(uint64_t width, uint64_t height);
void free_image(struct image* img);
void set_pixel(struct image* img, uint64_t x, uint64_t y, struct pixel const p);
struct pixel* get_pixel(struct image const* img, uint64_t x, uint64_t y);
void print_image_data(struct image const* img);

#endif //VIEW_HEADER_IMAGE_H
