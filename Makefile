CFLAGS=--std=c18 -O2 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
CC=gcc

all: print_header

bmp.o: src/bmp.c
	$(CC) -c $(CFLAGS) $< -o $@

util.o: src/util.c
	$(CC) -c $(CFLAGS) $< -o $@

image.o: src/image.c
	$(CC) -c $(CFLAGS) $< -o $@

image_transform.o: src/image_transform.c
	$(CC) -c $(CFLAGS) $< -o $@

main.o: src/main.c
	$(CC) -c $(CFLAGS) $< -o $@

print_header: main.o util.o bmp.o image.o image_transform.o
	$(CC) -o print_header $^

clean:
	rm -f main.o util.o bmp.o image.o print_header

